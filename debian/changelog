erlang-p1-mqtree (1.0.14-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Philipp Huebner ]
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Dec 2021 14:11:30 +0100

erlang-p1-mqtree (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 21:35:06 +0200

erlang-p1-mqtree (1.0.12-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 18:46:21 +0100

erlang-p1-mqtree (1.0.12-1) unstable; urgency=medium

  * New upstream version 1.0.12
  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 17:58:13 +0100

erlang-p1-mqtree (1.0.11-1) unstable; urgency=medium

  * New upstream version 1.0.11
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-mqtree.install

 -- Philipp Huebner <debalance@debian.org>  Fri, 25 Dec 2020 23:44:02 +0100

erlang-p1-mqtree (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 17:17:25 +0200

erlang-p1-mqtree (1.0.9-1) unstable; urgency=medium

  * New upstream version 1.0.9
  * Updated Erlang dependencies
  * Added libssl-dev to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 21:34:05 +0200

erlang-p1-mqtree (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 11:26:14 +0100

erlang-p1-mqtree (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 18:08:51 +0100

erlang-p1-mqtree (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5
  * Updated Standards-Version: 4.4.1 (no changes needed)
  * Updated debhelper compat version: 12
  * Fixed typo in long package description.
  * Set 'Rules-Requires-Root: no' in debian/control

 -- Philipp Huebner <debalance@debian.org>  Mon, 11 Nov 2019 16:04:24 +0100

erlang-p1-mqtree (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * Updated erlang dependencies
  * Modified lintian override to use version wildcard

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 21:28:34 +0200

erlang-p1-mqtree (1.0.3-4) unstable; urgency=medium

  * Added upstream patch to increase timeout for
    insert_then_delete_shuffle_test to fix FTBFS on mips
  * Added lintian-override for hardening-no-fortify-functions

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 21:18:51 +0200

erlang-p1-mqtree (1.0.3-3) unstable; urgency=medium

  * Source-only upload

 -- Philipp Huebner <debalance@debian.org>  Sat, 03 Aug 2019 08:25:20 +0200

erlang-p1-mqtree (1.0.3-2) unstable; urgency=medium

  * Corrected debian/rules to install mqtree.so into the binary package

 -- Philipp Huebner <debalance@debian.org>  Sat, 03 Aug 2019 08:21:19 +0200

erlang-p1-mqtree (1.0.3-1) unstable; urgency=medium

  * Initial release (Closes: #932800)

 -- Philipp Huebner <debalance@debian.org>  Tue, 23 Jul 2019 19:39:29 +0200
